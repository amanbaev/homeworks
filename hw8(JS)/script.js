const inputRef = document.querySelector(".js-price");
const resultRef = document.querySelector(".js-result");
const errorRef = document.querySelector(".js-error");
const buttonRef = document.querySelector(".js-close");

const inputFocusFn = () => {
    inputRef.style.border = "5px solid green";

    inputRef.value = "";
    errorRef.innerHTML = "";
    resultRef.innerHTML = "";
}

const validValue = () => {
    const value = inputRef.value;
    
    if(isNaN(value)) {
        errorRef.insertAdjacentHTML("beforeend", "It is not a number :(");

        return;
    };
    if(value < 0) {
        errorRef.insertAdjacentHTML("beforeend", "Please enter correct price");
        resultRef.innerHTML = ""
    } else {
        resultRef.insertAdjacentHTML("beforeend", `Текущая цена: ${value}`);
        inputRef.style.color = "green";
        buttonRef.style.display = "block";
        errorRef.innerHTML = "";
    };
};

const cleansingFn = () => {
    resultRef.innerHTML = "";
    inputRef.value = "";
    buttonRef.style.display = "none";
};

inputRef.addEventListener("focus", inputFocusFn);
inputRef.addEventListener("blur",validValue);
buttonRef.addEventListener("click", cleansingFn)
